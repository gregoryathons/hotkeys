# Javascript Hotkeys #

Install

```
npm install @teachingtextbooks/hotkeys
```

Usage, and properties

```
// import it
import Hotkeys from "@teachingtextbooks/hotkeys";

// enable Hotkeys, `true` by default
Hotkeys.enabled = true;

// outputs the keys pressed, `false` by default
Hotkeys.debug = false;

// switch keys so Control = Meta and Meta = Control depending on OS
// `true` by default, but have had better success with it set to `false`
Hotkeys.interchangableOS = false;

// listen for keys Control + A
function HeyThere() {
    console.log("Just saying hi!");
}
Hoykeys.on("control+a", HeyThere);
Hoykeys.on("control+a", () => { console.log("Another callback") });

// stop listening to a specific function, pass as the second parameter
Hotkeys.off("control+a", HeyThere);

// stop listening to ALL
Hotkeys.off("control+a");

// !NOTE!
// This will not work for ALL key combos
// for example the following combo will not work: Shift + 1
```