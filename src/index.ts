interface ICombo {
    ftn: Function;
    once: boolean;
    rule: number;
};

class Hotkeys {
    enabled = true;
    debug = false;
    interchangableOS = true;
    combos: {[combo: string]: Array<ICombo>} = {};
    keys: {[key: string]: boolean} = {};
    listenerCallback: Function | null = null;
    ruleCheckCallback: Function | null = null;

    constructor() {
        window.addEventListener("keydown", (event: Event) => this.onKeyDown(event as KeyboardEvent));
        window.addEventListener("keyup", (event: Event) => this.onKeyUp(event as KeyboardEvent));
        window.addEventListener("focus", () => this.onReset());
        window.addEventListener("blur", () => this.onReset());
    }

    private osCheck(combo: string) {
        if (this.interchangableOS) {
            if (navigator.userAgent.indexOf("Win") >= 0) {
                combo = combo.replace(/(meta)/gi, "control");
            }
            else if (navigator.userAgent.indexOf("Mac") >= 0) {
                combo = combo.replace(/(control)/gi, "meta");
            }
        }
        return combo;
    }

    on(combo: string, callback: Function, options: any = {}) {
        combo = this.osCheck(combo);
        let _combo = this.combos[combo];

        if (!_combo) {
            this.combos[combo] = [];
            _combo = this.combos[combo];
        }

        _combo.push({
            ftn: callback,
            once: options.once || false,
            rule: options.rule || 0
        } as ICombo);
    }

    off(combo: string, callback: Function | null = null) {
        combo = this.osCheck(combo);
        // remove all
        if (!callback) {
            delete this.combos[combo];
        }
        // remove specific
        else {
            let _combo = this.combos[combo];
            if (!_combo) {
                return;
            }

            const index = _combo.findIndex(c => c.ftn === callback);
            if (index >= 0) {
                _combo.splice(index, 1);
            }
        }
    }

    listener(callback: Function | null) {
        this.listenerCallback = callback;
    }

    ruleCheck(callback: Function | null) {
    	this.ruleCheckCallback = callback;
    }

    private checkCombos(event: KeyboardEvent) {
        if (!this.enabled) {
            return;
        }

        // get the keys
        let keys = Object.keys(this.keys).map(key => key).join("+") || "";

        // debug output
        if (this.debug) {
            console.log(`Hotkeys: ${keys}`);
        }

        // always listening
        this.listenerCallback && this.listenerCallback(keys);

        // get the rule
        let rule = (this.ruleCheckCallback && this.ruleCheckCallback()) || 0;
				
        // we find a combo with the keys and rule set
        let _combo = this.combos[keys];
        _combo = _combo && _combo.filter(combo => combo.rule === rule);
        if (!_combo) {
            return;
        }

        event.preventDefault();
        
        // release the keys once we found the combo
        Object.keys(this.keys).forEach(key => {
            if (key.length === 1) {
                delete this.keys[key];
            }
        });

        // execute for each one
        for (let i = 0; i < _combo.length; i++) {
            _combo[i].ftn(event, keys);

            // remove it
            if (_combo[i].once) {
                _combo.splice(i, 1);
                i--;
            }
        }
    }

    private filter(event: KeyboardEvent) {
        const target = (event.target || event.srcElement) as HTMLElement;
        const { tagName } = target;
        let flag = true;
        // ignore: isContentEditable === 'true', <input> and <textarea> when readOnly state is false, <select>
        if (target.isContentEditable || ((tagName === 'INPUT' || tagName === 'TEXTAREA' || tagName === 'SELECT') && !(target as any).readOnly)) {
            flag = false;
        }
        return flag;
    }

    private onKeyDown(event: KeyboardEvent) {
        const key = this.filter(event) ? (event.key || "").toLowerCase() : "";
        if (key) {
            this.keys[key] = true;
            this.checkCombos(event);
        }
    }

    private onKeyUp(event: KeyboardEvent) {
        const key = (event.key || "").toLowerCase();
        if (key) {
            delete this.keys[key];
        }
    }

    private onReset() {
        Object.keys(this.keys).forEach(key => delete this.keys[key]);
    }
}

const instance = new Hotkeys();

export default instance as Hotkeys;