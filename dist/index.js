"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
class Hotkeys {
    constructor() {
        this.enabled = true;
        this.debug = false;
        this.interchangableOS = true;
        this.combos = {};
        this.keys = {};
        this.listenerCallback = null;
        this.ruleCheckCallback = null;
        window.addEventListener("keydown", (event) => this.onKeyDown(event));
        window.addEventListener("keyup", (event) => this.onKeyUp(event));
        window.addEventListener("focus", () => this.onReset());
        window.addEventListener("blur", () => this.onReset());
    }
    osCheck(combo) {
        if (this.interchangableOS) {
            if (navigator.userAgent.indexOf("Win") >= 0) {
                combo = combo.replace(/(meta)/gi, "control");
            }
            else if (navigator.userAgent.indexOf("Mac") >= 0) {
                combo = combo.replace(/(control)/gi, "meta");
            }
        }
        return combo;
    }
    on(combo, callback, options = {}) {
        combo = this.osCheck(combo);
        let _combo = this.combos[combo];
        if (!_combo) {
            this.combos[combo] = [];
            _combo = this.combos[combo];
        }
        _combo.push({
            ftn: callback,
            once: options.once || false,
            rule: options.rule || 0
        });
    }
    off(combo, callback = null) {
        combo = this.osCheck(combo);
        // remove all
        if (!callback) {
            delete this.combos[combo];
        }
        // remove specific
        else {
            let _combo = this.combos[combo];
            if (!_combo) {
                return;
            }
            const index = _combo.findIndex(c => c.ftn === callback);
            if (index >= 0) {
                _combo.splice(index, 1);
            }
        }
    }
    listener(callback) {
        this.listenerCallback = callback;
    }
    ruleCheck(callback) {
        this.ruleCheckCallback = callback;
    }
    checkCombos(event) {
        if (!this.enabled) {
            return;
        }
        // get the keys
        let keys = Object.keys(this.keys).map(key => key).join("+") || "";
        // debug output
        if (this.debug) {
            console.log(`Hotkeys: ${keys}`);
        }
        // always listening
        this.listenerCallback && this.listenerCallback(keys);
        // get the rule
        let rule = (this.ruleCheckCallback && this.ruleCheckCallback()) || 0;
        // we find a combo with the keys and rule set
        let _combo = this.combos[keys];
        _combo = _combo && _combo.filter(combo => combo.rule === rule);
        if (!_combo) {
            return;
        }
        event.preventDefault();
        // release the keys once we found the combo
        Object.keys(this.keys).forEach(key => {
            if (key.length === 1) {
                delete this.keys[key];
            }
        });
        // execute for each one
        for (let i = 0; i < _combo.length; i++) {
            _combo[i].ftn(event, keys);
            // remove it
            if (_combo[i].once) {
                _combo.splice(i, 1);
                i--;
            }
        }
    }
    filter(event) {
        const target = (event.target || event.srcElement);
        const { tagName } = target;
        let flag = true;
        // ignore: isContentEditable === 'true', <input> and <textarea> when readOnly state is false, <select>
        if (target.isContentEditable || ((tagName === 'INPUT' || tagName === 'TEXTAREA' || tagName === 'SELECT') && !target.readOnly)) {
            flag = false;
        }
        return flag;
    }
    onKeyDown(event) {
        const key = this.filter(event) ? (event.key || "").toLowerCase() : "";
        if (key) {
            this.keys[key] = true;
            this.checkCombos(event);
        }
    }
    onKeyUp(event) {
        const key = (event.key || "").toLowerCase();
        if (key) {
            delete this.keys[key];
        }
    }
    onReset() {
        Object.keys(this.keys).forEach(key => delete this.keys[key]);
    }
}
const instance = new Hotkeys();
exports.default = instance;
