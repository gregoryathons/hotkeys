interface ICombo {
    ftn: Function;
    once: boolean;
    rule: number;
}
declare class Hotkeys {
    enabled: boolean;
    debug: boolean;
    interchangableOS: boolean;
    combos: {
        [combo: string]: Array<ICombo>;
    };
    keys: {
        [key: string]: boolean;
    };
    listenerCallback: Function | null;
    ruleCheckCallback: Function | null;
    constructor();
    private osCheck;
    on(combo: string, callback: Function, options?: any): void;
    off(combo: string, callback?: Function | null): void;
    listener(callback: Function | null): void;
    ruleCheck(callback: Function | null): void;
    private checkCombos;
    private filter;
    private onKeyDown;
    private onKeyUp;
    private onReset;
}
declare const _default: Hotkeys;
export default _default;
